package net.jc_mouse.domapp;

import android.util.Base64;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Guille on 8/7/2017.
 */

public class RequestHandler {



    // Metode untuk mengirim httpPostRequest
    // Metode ini mengambil dua argumen
    // Argumen Pertama adalah URL dari script yang kami akan mengirimkan permintaan
    // Lainnya adalah HashMap dengan nilai pasangan nama yang berisi data yang akan dikirim dengan permintaan
    public String sendPostRequest(String requestURL,
                                  HashMap<String, String> postDataParams) {
        //Creating a URL
        URL url;

        // Objek StringBuilder untuk review menyimpan Pesan diambil Dari Server
        StringBuilder sb = new StringBuilder();
        try {
            //Initializing Url
            url = new URL(requestURL+postDataParams.get("datos"));
            Log.e("RequestHandler",requestURL+postDataParams.get("datos"));
            //Membuat koneksi html url
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            // Konfigurasi properti koneksi
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            //Membuat Output
            OutputStream os = conn.getOutputStream();

            // Menulis parameter untuk permintaan
            // Kami menggunakan metode mendapatkan Posting data String yang didefinisikan di bawah ini
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(postDataParams));

            writer.flush();
            writer.close();
            os.close();
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {

                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                sb = new StringBuilder();
                String response;
                //Reading server response
                while ((response = br.readLine()) != null){
                    sb.append(response);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.e("RequestHandler",sb.toString());
        return sb.toString();
    }

    public String sendGetRequest(String requestURL){
        StringBuilder sb =new StringBuilder();
        try {
            URL url = new URL(requestURL);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            //Conect with auth
 //           con.setRequestProperty("Authorization", "basic " + Base64.encode("Domo:Domogrupo".getBytes(), Base64.NO_WRAP));
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

            String s;
            while((s=bufferedReader.readLine())!=null){
                sb.append(s+"\n");
            }
        }catch(Exception e){
        }
        return sb.toString();
    }

    public String sendGetRequestParam(String requestURL, String id){
        StringBuilder sb =new StringBuilder();
        try {
            URL url = new URL(requestURL+id);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

            String s;
            Log.w("en el handler","Hoooola");
            while((s=bufferedReader.readLine())!=null){
                sb.append(s+"\n");
            }
        }catch(Exception e){
        }
        Log.e("RequestHandler",sb.toString());
        return sb.toString();
    }

    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }
        Log.e("RequestHandler",result.toString());
        return result.toString();
    }
    public  ArrayList<Event> extractFeatureFromJson(String jsonString) {
        try {
            // If there are results in the features array
            JSONArray jsonArray = new JSONArray(jsonString);
            ArrayList<Event> arrayEvents = new ArrayList<Event>();
            for(int i=0;i<jsonArray.length();i++) {
                //JSONArray childJsonArray = jsonArray.getJSONArray(i);
                JSONObject contentJsonObject = jsonArray.getJSONObject(i);
             //   String id = contentJsonObject.getString("id");
                String accion = contentJsonObject.getString("accion");
                String fecha = contentJsonObject.getString("fecha");
                String tipo_actuador = contentJsonObject.getString("tipo_actuador");

                arrayEvents.add(i,new Event(accion,fecha,tipo_actuador));

                // Create a new {@link Event} object
            }
            return arrayEvents;
        } catch (JSONException e) {
            Log.e("RequestHandlerextracson", "Problem parsing the earthquake JSON results", e);
        }
        return null;
    }

    public  ArrayList<User> extractFeatureFromJsonUser(String jsonString) {
        try {
            // If there are results in the features array
            JSONArray jsonArray = new JSONArray(jsonString);
            ArrayList<User> arrayEvents = new ArrayList<User>();
            for(int i=0;i<jsonArray.length();i++) {
                //JSONArray childJsonArray = jsonArray.getJSONArray(i);
                JSONObject contentJsonObject = jsonArray.getJSONObject(i);
//                String id_user = contentJsonObject.getString("id_user");
//                String id_casa = contentJsonObject.getString("id_casa");
                String mail = contentJsonObject.getString("mail");
                String pass = contentJsonObject.getString("pass");
                String user = contentJsonObject.getString("user");
                arrayEvents.add(i,new User(mail,pass,user));
            }
            return arrayEvents;
        } catch (JSONException e) {
            Log.e("RequestHandlerextracson", "Problem parsing the earthquake JSON results", e);
        }
        return null;
    }
    public  ArrayList<History> extractFeatureFromJsonAll(String jsonString) {
        try {
            // If there are results in the features array
            JSONArray jsonArray = new JSONArray(jsonString);
            ArrayList<History> arrayEvents = new ArrayList<History>();
            for(int i=0;i<jsonArray.length();i++) {
                //JSONArray childJsonArray = jsonArray.getJSONArray(i);
                JSONObject contentJsonObject = jsonArray.getJSONObject(i);
                String usuario = contentJsonObject.getString("User");
                String accion = contentJsonObject.getString("accion");
                String fecha = contentJsonObject.getString("fecha");
                String tipo_actuador = contentJsonObject.getString("tipo_actuador");
                arrayEvents.add(i,new History(usuario,accion,fecha,tipo_actuador));

                // Create a new {@link Event} object
            }
            return arrayEvents;
        } catch (JSONException e) {
            Log.e("RequestHandlerextracson", "Problem parsing the earthquake JSON results", e);
        }
        return null;
    }
    public  ArrayList<Sensor> extractFeatureFromJsonSensor(String jsonString) {
        try {
            // If there are results in the features array
            JSONArray jsonArray = new JSONArray(jsonString);
            ArrayList<Sensor> arrayEvents = new ArrayList<Sensor>();
            for(int i=0;i<jsonArray.length();i++) {
                //JSONArray childJsonArray = jsonArray.getJSONArray(i);
                JSONObject contentJsonObject = jsonArray.getJSONObject(i);
                String cpuTemp = contentJsonObject.getString("cpuTemp");
                String ramTotal = contentJsonObject.getString("ram_total");
                String ramUsado = contentJsonObject.getString("ram_Used");
                String ramFree = contentJsonObject.getString("ram_Free");
                String discoTotal = contentJsonObject.getString("disk_Total");
                String discoFree = contentJsonObject.getString("disk_Free");
                String discoPrec = contentJsonObject.getString("disk_prec");

                arrayEvents.add(i,new Sensor(cpuTemp,ramTotal,ramUsado,ramFree,discoTotal,discoFree,discoPrec));

                // Create a new {@link Event} object
            }
            return arrayEvents;
        } catch (JSONException e) {
            Log.e("RequestHandlerextracson", "Problem parsing the earthquake JSON results", e);
        }
        return null;
    }

}