package net.jc_mouse.domapp;

/**
 * Created by Guille on 8/6/2017.
 */

public class Config {

    public static final String URL_INSERT="https://guarded-forest-48720.herokuapp.com/actuadores/?tipo_actuador=";
    public static final String URL_SELECT="https://guarded-forest-48720.herokuapp.com/actuadores";
    public static final String URL_SELECT_ALL="https://guarded-forest-48720.herokuapp.com/actuadores/all";
    public static final String URL_GETUSERS="https://guarded-forest-48720.herokuapp.com/usuarios";
    public static final String URL_GETSENSORES="https://guarded-forest-48720.herokuapp.com/usuarios";

    ///public static final String URL_INSERT="http://electronprog.com/domo/insert_luces.php?datos=";
    /*public static final String URL_INSERT="https://fast-hollows-94495.herokuapp.com/actuadores/?tipo_actuador=";
    public static final String URL_SELECT=" https://fast-hollows-94495.herokuapp.com/actuadores";
    public static final String URL_SELECT_ALL=" https://fast-hollows-94495.herokuapp.com/actuadores/all";
    public static final String URL_GETUSERS=" https://fast-hollows-94495.herokuapp.com/usuarios";*/
    /*public static final String URL_INSERT="http://10.0.2.2:5000/actuadores/?tipo_actuador=";
    public static final String URL_SELECT=" http://10.0.2.2:5000/actuadores";
    public static final String URL_SELECT_ALL="http://10.0.2.2:5000/actuadores/all";
    public static final String URL_GETUSERS="http://10.0.2.2:5000/usuarios";
*/
    public static final String URL_FAKE="http://localhost:3000/db";
    public static final String KEY_DATOS = "datos";
}
