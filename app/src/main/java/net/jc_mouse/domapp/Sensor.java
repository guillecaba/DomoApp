package net.jc_mouse.domapp;

/**
 * Created by Guillermo on 18/09/2017.
 */

public class Sensor {
    public  String cpuTemp;
    public  String ram_total;
    public  String ram_Used;
    public String ram_Free;
    public String disk_Total;
    public String disk_Free;
    public String disk_prec;


    public Sensor(String cpuTempE,String ram_totalE,String ram_UsedE ,String ram_FreeE,String disk_TotalE,String disk_FreeE,String disk_percE) {
        cpuTemp=cpuTempE;
        ram_total=ram_totalE;
        ram_Used=ram_UsedE;
        ram_Free=ram_FreeE;
        disk_Total=disk_TotalE;
        disk_Free=disk_FreeE;
        disk_prec=disk_percE;
    }
}
