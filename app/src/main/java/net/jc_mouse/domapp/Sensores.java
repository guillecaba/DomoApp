package net.jc_mouse.domapp;

import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class Sensores extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensores);

        //hilo para http request periodico
        final Handler handler = new Handler();
        Timer timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            DataAsyncTaskSensor taskSensor = new DataAsyncTaskSensor();
                            taskSensor.execute();
                            //task.cancel(true);
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                        }
                    }
                });
            }
        };
        timer.schedule(doAsynchronousTask, 0,4000);
    }
    private class DataAsyncTaskSensor extends AsyncTask<Void,Void,ArrayList<Sensor>> {
        @Override
        protected ArrayList<Sensor> doInBackground(Void... v) {
            RequestHandler rh = new RequestHandler();
            final String envio=new String();
            String res = rh.sendGetRequestParam(Config.URL_GETSENSORES,envio);
            ArrayList<Sensor>arraySensor=rh.extractFeatureFromJsonSensor(res);


            return arraySensor;
        }
        protected void onPostExecute(ArrayList<Sensor>result) {
            // If there is no result, do nothing.

            if (result == null) {
                return;
            }

            updateUi(result);
            if(isCancelled())
                return;
        }
    }
    private void updateUi(ArrayList<Sensor>arraySensor) {
        TextView cpuTextView = (TextView) findViewById(R.id.tw1);
        cpuTextView.setText(arraySensor.get(0).cpuTemp);

        TextView ramtotalTextView = (TextView) findViewById(R.id.tw2);
        ramtotalTextView.setText(arraySensor.get(0).ram_total);

        TextView ramusedTextView = (TextView) findViewById(R.id.tw3);
        ramusedTextView.setText(arraySensor.get(0).ram_Used);

        TextView ramfreeTextView = (TextView) findViewById(R.id.tw4);
        ramfreeTextView.setText(arraySensor.get(0).ram_Free);

        TextView dTotalTextView = (TextView) findViewById(R.id.tw5);
        dTotalTextView.setText(arraySensor.get(0).disk_Total);

        TextView diskfreeTextView = (TextView) findViewById(R.id.tw6);
        diskfreeTextView.setText(arraySensor.get(0).disk_Free);

        TextView diskprecTextView = (TextView) findViewById(R.id.tw7);
        diskfreeTextView.setText(arraySensor.get(0).disk_prec);

    }
}
