package net.jc_mouse.domapp;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Pattern;

/**
 * Created by Guillermo on 10/08/2017.
 */

public class LogsActivity extends AppCompatActivity implements View.OnClickListener {


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logs);

        Button btnupdate = (Button)findViewById(R.id.refresh);
        btnupdate.setOnClickListener(this);
        /*final Handler handler = new Handler();
        Timer timer = new Timer();

        //Voy a hacer una llamada comprobando cambios
        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            InformacionAsyncTask task = new InformacionAsyncTask();
                            task.execute();
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                        }
                    }
                });
            }
        };
        timer.schedule(doAsynchronousTask, 0, 1000); //execute in every 50000 ms*/

    }

    @Override
    public void onClick(View view) {
        InformacionAsyncTask task = new InformacionAsyncTask();
        task.execute();
    }


    private class InformacionAsyncTask extends AsyncTask<Void, Void, ArrayList<History>> {
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected ArrayList<History> doInBackground(Void... v) {
            RequestHandler rh = new RequestHandler();
            final String envio = new String();
            String res = rh.sendGetRequestParam(Config.URL_SELECT_ALL, envio);
            ArrayList<History> array = rh.extractFeatureFromJsonAll(res);
           return array;
        }

        protected void onPostExecute(ArrayList<History> result) {
            // If there is no result, do nothing.
            if (result == null) {
                return;
            }

            ActualizarUi(result);
        }
    }

    private void ActualizarUi(ArrayList<History> array) {
        Log.v("No mires: ", "Hola estoy aca");
        ArrayList<String> words = new ArrayList<String>();
        for (History i : array) {
            String[] f = i.fecha.split(Pattern.quote("."));
            if(i.accion.equals("1")) {
                String cadena = i.tipo_actuador + "     Prendido             "  + f[0]+"     "+i.usuario;
                if(i.tipo_actuador!=" ") {
                words.add(cadena);
                 }
            }else if (i.accion.equals("0")){
                String cadena = i.tipo_actuador + "     Apagado             " + f[0]+"     "+i.usuario;
                if(i.tipo_actuador!=" ") {
                    words.add(cadena);
                }
            }


        }
        Collections.reverse(words);
        ArrayAdapter<String> itemsAdapter = new ArrayAdapter<String>(this, R.layout.mytextview, words);


        ListView listView = (ListView) findViewById(R.id.list);

        listView.setAdapter(itemsAdapter);


    }
}

