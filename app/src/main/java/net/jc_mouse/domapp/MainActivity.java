package net.jc_mouse.domapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Inicializando TextView de Menu
        TextView luces =(TextView)findViewById(R.id.luces);
        TextView sensores =(TextView)findViewById(R.id.sensores);
        TextView logs =(TextView)findViewById(R.id.logs);
        TextView configuracion =(TextView)findViewById(R.id.Configuracion);

        //OnclikListeners de cada Activity
        luces.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent lucesIntent = new Intent(MainActivity.this,LucesActivity.class);
                startActivity(lucesIntent);
            }
        });
        logs.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent logsIntent = new Intent(MainActivity.this,LogsActivity.class);
                startActivity(logsIntent);
            }
        });

        configuracion.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent ConfiIntent = new Intent(MainActivity.this,ConfiguracionActivity.class);
                startActivity(ConfiIntent);
            }
        });
        sensores.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                Intent sensoresIntent = new Intent(MainActivity.this,Sensores.class);
                startActivity(sensoresIntent);
            }
        });
    }
}
