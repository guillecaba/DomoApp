package net.jc_mouse.domapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ConfiguracionActivity extends AppCompatActivity {
    Button btn1;
    EditText text1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configuracion);

        text1 =(EditText)findViewById(R.id.text1);
        btn1 =(Button)findViewById(R.id.btnSpeed);

        btn1.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view) {
                int dato = Integer.parseInt(text1.getText().toString());
                SharedPreferences preferences = getSharedPreferences("varios", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor =preferences.edit();
                editor.putInt("dato",dato);
                editor.commit();
            }
        });

    }
}
