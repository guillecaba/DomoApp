package net.jc_mouse.domapp;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Guille on 8/6/2017.
 */

public class LucesActivity extends AppCompatActivity implements View.OnClickListener {
    ToggleButton btn1;
    ToggleButton btn2;
    ToggleButton btn3;
    ToggleButton btn4;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_luces);
        //Inicializar el ToggleButton
        btn1 = (ToggleButton) findViewById(R.id.luz1);
        btn2 = (ToggleButton) findViewById(R.id.luz2);
        btn3 = (ToggleButton) findViewById(R.id.luz3);
        btn4 = (ToggleButton) findViewById(R.id.motor);

        SharedPreferences sharedPref = getSharedPreferences("prefe", Context.MODE_PRIVATE);

        SharedPreferences sharedPreference = getSharedPreferences("varios", Context.MODE_PRIVATE);
        int value = sharedPreference.getInt("dato",2000);
        Log.w("LucesActivity","Numero de value"+String.valueOf(value));







        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);

        //hilo para http request periodico
        final Handler handler = new Handler();
        Timer timer = new Timer();
        TimerTask doAsynchronousTask = new TimerTask() {
           @Override
            public void run() {
            handler.post(new Runnable() {
                    public void run() {
                        try {
                            DataAsyncTask task = new DataAsyncTask();
                            task.execute();
                            //task.cancel(true);
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                        }
                    }
                });
            }
        };
        timer.schedule(doAsynchronousTask, 0,value); //ejercuta cada 5000ms osea 5 segundos*/
        //DataAsyncTask task = new DataAsyncTask();
        //task.execute();
        }







    private class DataAsyncTask extends AsyncTask<Void,Void,ArrayList<Event>>{
        @Override
        protected ArrayList<Event> doInBackground(Void... v) {
            RequestHandler rh = new RequestHandler();
            final String envio=new String();
            String res = rh.sendGetRequestParam(Config.URL_SELECT,envio);
            ArrayList<Event>array=rh.extractFeatureFromJson(res);


            return array;
        }
        protected void onPostExecute(ArrayList<Event>result) {
            // If there is no result, do nothing.

            if (result == null) {
                return;
            }

            updateUi(result);
            if(isCancelled())
                return;
        }
    }
    private void updateUi(ArrayList<Event>array) {
        Log.v("No mires: ","Hola estoy aca");
        if(array.get(0).accion.compareTo("1")==0){
            ToggleButton btn1ToggleButton = (ToggleButton) findViewById(R.id.luz1);
            btn1ToggleButton.setChecked(true);
           // btn1ToggleButton.setTextOn("On");
            Log.v("No mires: ","si entre nde puta");
        }else {
            ToggleButton btn1ToggleButton = (ToggleButton) findViewById(R.id.luz1);
            btn1ToggleButton.setChecked(false);
            //btn1ToggleButton.setTextOff("off");
        }
        if(array.get(1).accion.compareTo("1")==0){
            ToggleButton btn2ToggleButton = (ToggleButton) findViewById(R.id.luz2);
            btn2ToggleButton.setChecked(true);
           // btn2ToggleButton.setTextOn("On");
        }else {
            ToggleButton btn2ToggleButton = (ToggleButton) findViewById(R.id.luz2);
            btn2ToggleButton.setChecked(false);
            //btn2ToggleButton.setTextOff("off");
        }
        if(array.get(2).accion.compareTo("1")==0){
            ToggleButton btn3ToggleButton = (ToggleButton) findViewById(R.id.luz3);
            btn3ToggleButton.setChecked(true);
          //  btn3ToggleButton.setTextOn("On");
        }else {
            ToggleButton btn3ToggleButton = (ToggleButton) findViewById(R.id.luz3);
            btn3ToggleButton.setChecked(false);
           // btn3ToggleButton.setTextOff("Off");
        }
        if(array.get(3).accion.compareTo("1")==0){
            ToggleButton btn4ToggleButton = (ToggleButton) findViewById(R.id.motor);
            btn4ToggleButton.setChecked(true);
            //  btn3ToggleButton.setTextOn("On");
        }else {
            ToggleButton btn4ToggleButton = (ToggleButton) findViewById(R.id.motor);
            btn4ToggleButton.setChecked(false);
            // btn3ToggleButton.setTextOff("Off");
        }
    }

    //cambiarLuz
    private void cambiarLuz(String cadena){

        final String luz = cadena;
        boolean check=false;
        Log.w("LucesActivity",luz);
        final String accion;
        if(cadena.equals("luz1")){
            check=btn1.isChecked();
        }
        if(cadena.equals("luz2")){
            check=btn2.isChecked();
        }
        if(cadena.equals("luz3")){
            check=btn3.isChecked();
        }
        if(cadena.equals("motor")){
            check=btn4.isChecked();
        }

        if(check){
            accion="1";
        }else{
            accion="0";
        }
        SharedPreferences sharedPref = getSharedPreferences("prefe", Context.MODE_PRIVATE);
        final String envio=luz+"&accion="+accion+"&usuario="+sharedPref.getString((getString(R.string.user)), "");
        String prueba=envio+"&usuario="+sharedPref.getString((getString(R.string.user)), "");
        Log.w("LucesActivity",prueba);




        class CambiarLuz extends AsyncTask<Void,Void,String> {

            @Override
            protected String doInBackground(Void... v) {
                //HashMap<String,String> params = new HashMap<>();
               // params.put(Config.KEY_DATOS,envio);

                Log.w("LucesActivity","Hoooola");
                RequestHandler rh = new RequestHandler();
                String res = rh.sendGetRequestParam(Config.URL_INSERT, envio);


                return res;
            }
        }

        CambiarLuz ae = new CambiarLuz();
        ae.execute();
    }
    @Override
    public void onClick(View v) {
        if(v == btn1){
            Log.w("LucesActivity","Entre en el boton 11111");
            cambiarLuz("luz1");
        }
        if(v == btn2){
            Log.w("LucesActivity","Entre en el boton 2");
            cambiarLuz("luz2");
        }
        if(v == btn3){
            Log.w("LucesActivity","Entre en el boton 3");
            cambiarLuz("luz3");
        }
        if(v == btn4){
            Log.w("LucesActivity","Entre en el boton 4");
            cambiarLuz("motor");
        }




    }


}
