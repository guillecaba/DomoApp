package net.jc_mouse.domapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import java.util.ArrayList;


/**
 * Created by N on 12/8/2017.
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    EditText userName, userPass;
    Switch showPass;
    Button entrar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        userName = (EditText) findViewById(R.id.userName);
        userPass = (EditText) findViewById(R.id.userPass);
        showPass = (Switch) findViewById(R.id.showPass);
        entrar = (Button) findViewById(R.id.ingresar);
        entrar.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        if (v == entrar) {
            Log.w("LucesActivity", "Entre en el boton 11111");
            abrirActivity(userName.getText().toString(), userPass.getText().toString());
        }
    }
    private void abrirActivity(final String user, final String pass){


        class Cambiar extends AsyncTask<Void,Void,String> {

            @Override
            protected String doInBackground(Void... v) {
                //HashMap<String,String> params = new HashMap<>();
                // params.put(Config.KEY_DATOS,envio);
                String envio = new String();
                Log.w("LucesActivity","Hoooola");
                RequestHandler rh = new RequestHandler();

                String res = rh.sendGetRequestParam(Config.URL_GETUSERS, envio);
                ArrayList<User> array=rh.extractFeatureFromJsonUser(res);
                Boolean login=false;
                for (int i=0;array.size()>i;i++){
                    if (user.equals(array.get(i).user) && pass.equals(array.get(i).pass)) {
                        login = true;
                        SharedPreferences sharedPref = getSharedPreferences("prefe", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        //editor.putString(getString(R.string.user), array.get(i).id);
                        editor.putString(getString(R.string.user), array.get(i).user);
                        //Log.w("LoginActivity",array.get(i).id);
                        Log.w("LoginActivity",array.get(i).user);
                        editor.commit();
                    }

                    if (login) {
                        Intent familyIntent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(familyIntent);
                    }else{

                    }
                }

                return res;
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
            }
        }

        Cambiar ae = new Cambiar();
        ae.execute();
    }

}
